package main

const (
	// Path for CBG files $HOME/.config/cbg/
	CONFIG_CBG string = ".config/cbg/"
)

var (
	showHelp = map[string]string{
		"show": "shows variables and their current values",

		"<variable>": "inverts current boolean <variable>",
		"exit":       "exit the CLI",
	}

	OPENAI_PROVIDER = &Provider{
		Name:               "OpenAI",
		ApiKeyVariableName: "OPENAI_API_KEY",
	}

	ANTHROPIC = &Provider{
		Name:               "Anthropic",
		ApiKeyVariableName: "ANTHROPIC_API_KEY",
	}

	// Model list OpenAI: https://github.com/sashabaranov/go-openai/blob/v1.28.1/completion.go#L19
	GPT_3_5_TURBO = &Model{
		Name:     "gpt-3.5-turbo",
		Provider: OPENAI_PROVIDER,
	}

	GPT_4 = &Model{
		Name:     "gpt-4",
		Provider: OPENAI_PROVIDER,
	}

	GPT_4O = &Model{
		Name:     "gpt-4o",
		Provider: OPENAI_PROVIDER,
	}

	GPT_4_TURBO = &Model{
		Name:     "gpt-4-turbo",
		Provider: OPENAI_PROVIDER,
	}

	CLAUDE_3_HAIKU = &Model{
		Name:     "claude-3-haiku-20240307",
		Provider: ANTHROPIC,
	}

	CLAUDE_2_0 = &Model{
		Name:     "claude-2.0",
		Provider: ANTHROPIC,
	}

	MODELS = []*Model{
		GPT_3_5_TURBO,
		GPT_4,
		GPT_4O,
		GPT_4_TURBO,
		CLAUDE_3_HAIKU,
		CLAUDE_2_0,
	}

	confDir    string
	promptsDir string = "prompts"
)
